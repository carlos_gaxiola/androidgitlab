package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1, txtNum2, txtRes;
    private Button btnSuma, btnResta, btnMulti, btnDivi, btnLimpiar, btnCerrar;
    private Operaciones op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setListeners();
    }

    private void initComponents () {
        this.txtNum1 = findViewById(R.id.txtNum1);
        this.txtNum1 = findViewById(R.id.txtNum2);
        this.txtRes = findViewById(R.id.txtRes);
        this.btnSuma = findViewById(R.id.btnSuma);
        this.btnDivi = findViewById(R.id.btnDivi);
        this.btnResta = findViewById(R.id.btnResta);
        this.btnMulti = findViewById(R.id.btnMult);
        this.btnCerrar = findViewById(R.id.btnCerrar);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
        this.op = new Operaciones(0.0f, 0.0f);
    }

    private void setListeners () {
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.btnSuma: sumar(); break;
            case R.id.btnDivi: dividir(); break;
            case R.id.btnResta: restar(); break;
            case R.id.btnMult: multiplicar(); break;
            case R.id.btnCerrar: cerrar(); break;
            case R.id.btnLimpiar: limpiar(); break;
        }
    }

    private void limpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        txtRes.setText("");
    }

    private void cerrar() { System.exit(0); }

    private void multiplicar() {
        setNums();
        float res = op.mult();
        showRes(res);
    }

    private void dividir() {
        setNums();
        float res = op.div();
        showRes(res);
    }

    private void restar() {
        setNums();
        float res = this.op.resta();
        showRes(res);
    }

    private void sumar() {
        setNums();
        float res = this.op.suma();
        showRes(res);
    }

    private void showRes(float res) { this.txtRes.setText(String.valueOf(res)); }

    private void setNums () {
        this.op.setNum1(Integer.parseInt(this.txtNum1.getText().toString()));
        this.op.setNum2(Integer.parseInt(this.txtNum2.getText().toString()));
    }

    //Comentario para el commit
    //Otro comentario 
}